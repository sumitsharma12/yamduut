class ApplicationMailer < ActionMailer::Base
  default from: 'ss828863@gmail.com'
  layout "mailer"
end
