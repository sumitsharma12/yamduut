class GmailMailer < ApplicationMailer
  def new_list_email(user,title)
    mail(to: user, subject: title)
  end

  def test_email
    mail(to: 'demotest@yopmail.com', subject: 'Test Email', body: 'This is a test email.')
  end
end
