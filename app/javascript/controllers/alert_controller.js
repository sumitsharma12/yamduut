import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="alert"
export default class extends Controller {
  static targets = ["alert"]

  connect() {
    this.autoClose(5000); // Auto close after 5 seconds (5000 milliseconds)
  }

  close() {
    this.alertTarget.style.display = 'none';
  }

  autoClose(duration) {
    setTimeout(() => {
      this.close();
    }, duration);
  }
}
