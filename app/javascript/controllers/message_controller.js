import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="message"
export default class extends Controller {

  static targets = ["header", "content"]

  fillForm(event) {
    const template = event.currentTarget.dataset
    this.headerTarget.value = template.header
    this.contentTarget.value = template.content
  }
}
