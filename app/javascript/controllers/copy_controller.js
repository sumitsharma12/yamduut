import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = [ "name" ]

  copy(event) {
    event.preventDefault()
    const name = event.currentTarget.dataset.name

    navigator.clipboard.writeText(name).catch(err => {
      console.error('Could not copy text: ', err)
    })
  }
}
