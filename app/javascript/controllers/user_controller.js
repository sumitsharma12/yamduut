import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["checkbox"];

  connect() {
    this.csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
  }

  toggle(event) {
    const checkbox = event.target;
    const userId = checkbox.dataset.userId;
    const isMessageSent = checkbox.checked;

    fetch(`/users/${userId}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        "X-CSRF-Token": this.csrfToken
      },
      body: JSON.stringify({ user: { is_message_sent: isMessageSent } })
    })
    .then(response => {
      if (response.ok) {
        // alert("User updated successfully");
      } else {
        alert("Failed to update user");
        response.json().then(data => console.error(data)); 
      }
    })
    .catch(error => {
      console.error(`Failed to update user ${userId}`, error);
    });
  }
}
