class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?


  # Override the path after sign in
  def after_sign_in_path_for(resource)
    users_path # Use a named route or a string path
  end

  # Override the path after sign up
  def after_sign_up_path_for(resource)
    users_path # Use a named route or a string path
  end

  # Override the path after inactive sign up
  def after_inactive_sign_up_path_for(resource)
    users_path # Use a named route or a string path if user is inactive
  end
 
  def check_organization_login
    unless organization_signed_in?
      redirect_to root_path, alert: 'Please log in to continue.'
    end
  end
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end
end
