class ShortCodesController < ApplicationController
  before_action :check_organization_login  
  before_action :set_short_code, only: %i[show edit update destroy]
  
  def index
    @short_codes = ShortCode.all
  end

  def show
  end

  def new
    @short_code = ShortCode.new
  end

  def edit
  end

  def create
    @short_code = ShortCode.new(short_code_params)

    if @short_code.save
      redirect_to @short_code, notice: 'Short code was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @short_code.update(short_code_params)
      redirect_to @short_code, notice: 'Short code was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    if @short_code.destroy
      redirect_to short_codes_url, notice: 'Short code was successfully destroyed.'
    else
      redirect_to @short_code, alert: @short_code.errors.full_messages.to_sentence
    end
  end

  private

  def set_short_code
    @short_code = ShortCode.find(params[:id])
  end

  def short_code_params
    params.require(:short_code).permit(:name, :expression)
  end
end
