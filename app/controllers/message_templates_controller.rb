class MessageTemplatesController < ApplicationController
    before_action :check_organization_login
    before_action :set_template, only: [:edit, :update, :destroy]
  
    def index
      @templates = current_organization.message_templates rescue []
    end
  
    def new
      @template = current_organization.message_templates.build
    end
  
    def edit
    end
  
    def create
      @template = current_organization.message_templates.build(template_params)
      @template.save
      respond_to do |format|
        format.turbo_stream 
      end
    end
  
    def update
      respond_to do |format|
        if @template.update(template_params)
          format.turbo_stream
        else
          format.turbo_stream do
            render turbo_stream: turbo_stream.replace('form_errors', partial: 'message_templates/form_errors', locals: { template: @template })
          end
        end
      end
    end
  
    def destroy
      @template.destroy
      respond_to do |format|
        format.turbo_stream 
      end
    end
  
    private

    def check_organization_login
      unless organization_signed_in?
        redirect_to root_path, alert: 'Please log in to continue.'
      end
    end
  
    def set_template
      @template = current_organization.message_templates.find(params[:id])
    end
  
    def template_params
      params.require(:message_template).permit(:title, :content, :organizations_id)
    end
end
