class MessagesController < ApplicationController
  before_action :check_organization_login

  def index
    @templates = current_organization.message_templates rescue []
  end

  def new
    @message = Message.new
  end

  def create
    if params[:header].present? && params[:content].present?
      user_ids = current_organization.users.where(is_message_sent: true).pluck(:id)
      BulkMessageWorker.perform_async(params[:content], params[:header], user_ids)  
    end
  end

  private

  
    def check_organization_login
      unless organization_signed_in?
        redirect_to root_path, alert: 'Please log in to continue.'
      end
    end
end
