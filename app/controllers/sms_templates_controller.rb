class SmsTemplatesController < ApplicationController
  before_action :check_organization_login  
  before_action :set_sms_template, only: %i[show edit update destroy]
  
    def index
      @sms_templates = SmsTemplate.all
    end
  
    def show
        @sample_user = User.first
    end
  
    def new
      @sms_template = SmsTemplate.new
      @short_codes = ShortCode.all
    end
  
    def edit
        @short_codes = ShortCode.all
    end
  
    def create
      @sms_template = SmsTemplate.new(sms_template_params)
  
      if @sms_template.save
        redirect_to @sms_template, notice: 'Sms template was successfully created.'
      else
        render :new, status: :unprocessable_entity
      end
    end
  
    def update
      if @sms_template.update(sms_template_params)
        redirect_to @sms_template, notice: 'Sms template was successfully updated.'
      else 
        render :edit, status: :unprocessable_entity
      end
    end
  
    def destroy
      @sms_template.destroy
      redirect_to sms_templates_url, notice: 'Sms template was successfully destroyed.'
    end
  
    private
  
    def set_sms_template
      @sms_template = SmsTemplate.find(params[:id])
    end
  
    def sms_template_params
      params.require(:sms_template).permit(:name, :content)
    end
  end
  