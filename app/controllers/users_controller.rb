class UsersController < ApplicationController
  before_action :check_organization_login
  before_action :set_user, only: [:destroy, :edit, :update]

  def index
    @users = current_organization.users
  end

  def new
    @user = current_organization.users.build
  end

  def create
    @user = current_organization.users.build(user_params)
    if @user.save
      respond_to do |format|
        format.turbo_stream
      end
    else
      respond_to do |format|
        format.turbo_stream do
          render turbo_stream: turbo_stream.replace('form_errors', partial: 'users/form_errors', locals: { user: @user })
        end
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.turbo_stream { render turbo_stream: turbo_stream.remove(@user) }
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.turbo_stream
      else
        format.turbo_stream do
          render turbo_stream: turbo_stream.replace('form_errors', partial: 'users/form_errors', locals: { user: @user })
        end
      end
    end
  end

  private

  def set_user
    @user = current_organization.users.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :number, :is_message_sent, :email)
  end
end
