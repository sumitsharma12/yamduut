class SendTwilioMessageWorker
    include Sidekiq::Worker
    sidekiq_options retry: false
  
    def perform(to, body)
      client = Twilio::REST::Client.new
      client.messages.create(
        from: '+14175453345', # Your Twilio phone number
        to: to,
        body: body
      )
    rescue Twilio::REST::RestError => e
      Rails.logger.error "Twilio error: #{e.message}"
    end
  end
  