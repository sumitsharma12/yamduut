class CreateMessageRecordWorker
    include Sidekiq::Worker
    sidekiq_options retry: false
  
    def perform(user_id, message, title)
      Message.create(user_id: user_id, title: title, content: message)
    rescue StandardError => e
      Rails.logger.error "Failed to create message record for user #{user_id}: #{e.message}"
    end
  end
  