class SendWhatsAppMessageWorker
    include Sidekiq::Worker
    sidekiq_options retry: false
  
    def perform(to)
      HTTParty.post(
        'https://graph.facebook.com/v19.0/320044137866503/messages',
        headers: {
          'Authorization' => 'Bearer EAAOWPUIflsABOZC2wezlYeAZBrE3DWMWuZBZCOQFJ0kKWyPXdwlw1HkefONR3uugjJ1WYLm9kP7aZCEhHExx8ZBkoxbpMLxf0HfNI8bpiJ7bKYYnIH7waF3EdUfNuH0STBp6pUJlqttGf8g87rVowhSkYA4K6D1wZBoU1cmjOOR99ZA8JSwZCy729iKtKl9kgsJWlCTUSUuXzIHMZAc59kAMjSWKseYzZAk4WmHv4UZD',
          'Content-Type' => 'application/json'
        },
        body: {
          messaging_product: "whatsapp",
          to: to,
          type: "template",
          template: {
            name: "hello_world",
            language: {
              code: "en_US"
            }
          }
        }.to_json
      )
    rescue HTTParty::Error => e
      Rails.logger.error "WhatsApp API error: #{e.message}"
    end
  end
  