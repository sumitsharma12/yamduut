class BulkMessageWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(message, title, user_ids)
    users = User.where(id: user_ids)
    users.each do |user|
      begin
        SendTwilioMessageWorker.perform_async(user.number, message)
        SendWhatsAppMessageWorker.perform_async(user.number)
        CreateMessageRecordWorker.perform_async(user.id, message, title)
        GmailMailer.new_list_email(user.email, message).deliver_now
      rescue StandardError => e
        # Log the error for debugging purposes
        Rails.logger.error "Failed to queue jobs for user #{user.id}: #{e.message}"
        next
      end
    end
  end
end
