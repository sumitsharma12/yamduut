# app/services/twilio_service.rb
class TwilioService
    def initialize
      @client = Twilio::REST::Client.new
    end
  
    def send_sms(to:, body:)
      @client.messages.create(
        from: '+14155238886',
        to: to,
        body: body
      )
    rescue Twilio::REST::RestError => e
      Rails.logger.error "Twilio Error: #{e.message}"
      false
    end
 end
  