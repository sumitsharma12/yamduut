class User < ApplicationRecord
    validates :name, presence: true
    validates :number, presence: true
    validates :number, format: { with: /\A\+\d{1,3}\d{4,14}\z/, message: "must be in the format '+[Country Code][Subscriber Number]', e.g., '+919876543210'" }
    belongs_to :organization
end
