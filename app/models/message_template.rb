class MessageTemplate < ApplicationRecord
    # Associations
    belongs_to :organization
  
    # Validations
    validates :title, presence: true, length: { minimum: 5, maximum: 100 }
    validates :content, presence: true
    validates :organization_id, presence: true
  end
  