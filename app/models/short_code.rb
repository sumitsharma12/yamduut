class ShortCode < ApplicationRecord
  before_save :format_name
  before_destroy :check_if_used_in_sms_template

  validates :name, presence: true
  validates :expression, presence: true

  private

  def format_name
    self.name = "%#{name}%" unless name.start_with?('%') && name.end_with?('%')
  end

  def check_if_used_in_sms_template
    SmsTemplate.find_each do |template|
      if template.content.include?(self.name)
        errors.add(:base, "Cannot delete a ShortCode that is used in an SmsTemplate")
        throw(:abort)
      end
    end
  end
end
