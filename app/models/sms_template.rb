class SmsTemplate < ApplicationRecord
    validates :name, presence: true
    validates :content, presence: true

    def render_template(user)
        rendered_content = content.dup
        rendered_title = name.dup
        ShortCode.all.each do |short_code|
          value = eval("#{short_code.expression}")
          rendered_content.gsub!(short_code.name, value.to_s)
          rendered_title.gsub!(short_code.name, value.to_s)
        end
        [ rendered_content, rendered_title ]
    end
end
