class CreateShortCodes < ActiveRecord::Migration[7.1]
  def change
    create_table :short_codes do |t|
      t.string :name
      t.text :expression

      t.timestamps
    end
  end
end
