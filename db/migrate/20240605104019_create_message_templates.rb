class CreateMessageTemplates < ActiveRecord::Migration[7.1]
  def change
    create_table :message_templates do |t|
      t.string :title
      t.text :content
      t.references :organization

      t.timestamps
    end
  end
end
