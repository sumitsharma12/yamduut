class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :number
      t.boolean :is_message_sent
      t.integer :organization_id

      t.timestamps
    end
  end
end
