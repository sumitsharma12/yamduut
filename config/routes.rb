Rails.application.routes.draw do
  root "homepage#index"
  devise_for :organizations
  resources :users
  resources :messages
  resources :message_templates
  resources :sms_templates
  resources :short_codes
end
